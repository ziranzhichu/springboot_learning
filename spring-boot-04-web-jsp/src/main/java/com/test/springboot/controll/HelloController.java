package com.test.springboot.controll;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author: ybl
 * @create: 2019-05-16 21:55
 **/
@Controller
public class HelloController {

    @GetMapping("hello")
    public String hello(Model model) {
        model.addAttribute("msg", "are you ok");
        return "success";
    }
}
