package cn.test.springboot.controller;

import cn.test.springboot.model.Department;
import cn.test.springboot.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @create: 2019-07-07 17:28
 **/
@RestController
@RequestMapping("dept")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("{id}")
    public Department queryDept(@PathVariable("id") Integer id) {
        return departmentService.getById(id);
    }
}
