package cn.test.springboot.service;

import cn.test.springboot.mapper.DepartmentMapper;
import cn.test.springboot.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @create: 2019-07-07 18:54
 **/
@Service
public class DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    public Department getById(Integer id) {
        return departmentMapper.selectByPrimaryKey(id);
    }


    @Transactional(rollbackFor = Exception.class)
    public void deleteById(Integer id) {
        departmentMapper.deleteByPrimaryKey(id);
    }

}
