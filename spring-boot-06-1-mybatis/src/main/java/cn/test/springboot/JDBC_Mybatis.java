package cn.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 使用通用mapper的方式来使用mybatis,并整合事务。
 * 因为数据库的命名问题，这里的访问会出错。所以这里只作演示
 *
 * @create: 2019-07-07 17:51
 **/
@SpringBootApplication
//@MapperScan(value = "cn.test.springboot.mapper") 开启扫描mapper接口
public class JDBC_Mybatis {
    public static void main(String[] args) {
        SpringApplication.run(JDBC_Mybatis.class, args);
    }
}
