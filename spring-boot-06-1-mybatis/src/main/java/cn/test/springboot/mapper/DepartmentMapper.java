package cn.test.springboot.mapper;

import cn.test.springboot.model.Department;
import tk.mybatis.mapper.common.Mapper;

/**
 * @create: 2019-07-07 17:57
 **/
@org.apache.ibatis.annotations.Mapper
public interface DepartmentMapper extends Mapper<Department> {
}
