package com.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot03WebRestfulCurdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot03WebRestfulCurdApplication.class, args);
    }

}
