package com.test.springboot.component;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录检查拦截器,
 * 注意,拦截器写好后,需要添加到mvc配置中,使用添加拦截器
 *
 * @author: ybl
 * @create: 2019-05-15 13:45
 **/
public class LoginHandlerInterceptor implements HandlerInterceptor {

    //目标方法执行之前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user = request.getSession().getAttribute("loginUser");
        if (user == null) {
            //未登录,返回登录页面
            request.setAttribute("msg", "没有权限,请先登录!");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return false;
        } else {
            //已登录,放行请求
            return true;
        }


    }
}
