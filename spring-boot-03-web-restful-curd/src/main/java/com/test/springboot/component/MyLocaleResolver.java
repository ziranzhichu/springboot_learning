package com.test.springboot.component;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 可以在连接上携带区域信息
 * 该类主要用来获取请求上的语言类型参数,并根据类型参数来设置区域信息,从而达到国际化的效果
 *
 * @author: ybl
 * @create: 2019-05-12 15:41
 **/
public class MyLocaleResolver implements LocaleResolver {

    //根据请求内容来设置区域信息(locale)
    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        //获取请求体内容
        String l = httpServletRequest.getParameter("l");
        //创建区域locale并设置为默认
        Locale locale = Locale.getDefault();
        if (!StringUtils.isEmpty(l)) {
            //判断请求内容,并根据内容来创建区域信息
            String[] split = l.split("_");
            locale = new Locale(split[0], split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}
