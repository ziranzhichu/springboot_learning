package com.test.springboot.component;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 给容器中添加我们自己定义的errorAttributes
 * 这样就可以在异常时返回给客户端时带上自定义的参数
 *
 * @author: ybl
 * @create: 2019-05-16 01:09
 **/
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {
    //返回值的map就是页面和json获取的所有字段
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {

        Map<String, Object> map = super.getErrorAttributes(webRequest, includeStackTrace);
        map.put("company", "atguigu");

        //我们自定义处理器携带的参数
        Map<String, Object> ext = (Map<String, Object>) webRequest.getAttribute("ext", 0);

        //将自定义的参数也封装到异常参数域中
        map.put("ext", ext);
        return map;
    }
}
