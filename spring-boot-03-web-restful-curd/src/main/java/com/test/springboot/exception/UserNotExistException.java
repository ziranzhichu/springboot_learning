package com.test.springboot.exception;

/**
 * 自定义用户不存在异常
 *
 * @author: ybl
 * @create: 2019-05-16 00:22
 **/
public class UserNotExistException extends RuntimeException {

    public UserNotExistException() {
        super("用户不存在!");
    }
}
