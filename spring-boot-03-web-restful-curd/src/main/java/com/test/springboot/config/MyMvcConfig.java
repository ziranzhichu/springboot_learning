package com.test.springboot.config;

import com.test.springboot.component.MyLocaleResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 自定义的mvc配置类,配置一些自己需要的配置,
 * 必须实现WebMvcConfigurer接口.并重写需要的方法,主要就是addViewControllers方法
 * 实现WebMvcConfigurer接口可以来扩展springMVC的功能,原来继承的WebMvcConfigurerAdapter已过时
 * 即保留了所有的自动配置,也扩展了一些我们自己的配置
 * EnableWebMvc注解,如果在配置类上添加了这个注解,那么就表示我们不再使用springboot自动配置的springMVC配置了.改为我们来配置MVC相关配置
 *
 * @author: ybl
 * @create: 2019-05-12 02:51
 **/
//@EnableWebMvc
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    //添加指定的请求跳转视图
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        super.addViewControllers(registry);

        //浏览器发送一个请求atguigu,在这里指定跳转的页面为success
        registry.addViewController("/atguigu").setViewName("success");
    }

    //在配置类中手动配置主页的请求
    @Bean  //注意需要把这个组件添加到spring容器中,否则会不起作用
    public WebMvcConfigurer webIndex() {
        WebMvcConfigurer adapter = new WebMvcConfigurer() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");
            }

            //注册拦截器
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //添加拦截器,并添加addPathPatterns指定需要拦截的请求,最后加上需要放行的请求excludePathPatterns
                //springboot已经做好了静态资源映射,这里不再需要再写了
//                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns("/index.html", "/", "/user/login");
            }
        };

        return adapter;
    }

    //将我们自己的区域信息添加到容器中,这样便可以取代springboot默认的
    @Bean
    public LocaleResolver localeResolver() {
        return new MyLocaleResolver();
    }

}
