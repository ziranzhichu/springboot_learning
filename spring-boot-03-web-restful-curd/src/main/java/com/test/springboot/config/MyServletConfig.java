package com.test.springboot.config;

import com.test.springboot.filter.MyFilter;
import com.test.springboot.listener.MyListener;
import com.test.springboot.servlet.MyServlet;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Servlet;
import java.util.Arrays;

/**
 * @author: ybl
 * @create: 2019-05-16 14:48
 **/
@Configuration
public class MyServletConfig {

    /*
    注册三大组件
     */
    //注册servlet容器
    @Bean
    public ServletRegistrationBean myServlet() {
        //将自定义的容器载入
        ServletRegistrationBean<Servlet> bean = new ServletRegistrationBean<>(new MyServlet(), "/myServlet");
        return bean;
    }

    //注册filter过滤器
    @Bean
    public FilterRegistrationBean myFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();
        //指定过滤器及需要过滤的url
        bean.setFilter(new MyFilter());
        bean.setUrlPatterns(Arrays.asList("/hello", "/myServlet"));
        return bean;
    }

    //注册监听器
    @Bean
    public ServletListenerRegistrationBean<MyListener> myListener() {
        ServletListenerRegistrationBean<MyListener> bean = new ServletListenerRegistrationBean<>(new MyListener());
        return bean;
    }

    //配置嵌入式的servlet容器
    @Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {

            //定制嵌入式的servlet容器相关规则
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                factory.setPort(8083);
            }
        };
    }


}
