package com.test.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 登录相关的controller请求处理类
 *
 * @author: ybl
 * @create: 2019-05-15 13:01
 **/
@Controller
public class LoginController {

    //简捷的请求映射注解写法
//    @DeleteMapping()
//    @PutMapping()
//    @GetMapping()
//    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    @PostMapping(value = "/user/login")
    public String login(@RequestParam("username") String username, @RequestParam("password") String password, Map<String, Object> map, HttpSession session) {
        //判断用户名及密码
        if (!StringUtils.isEmpty(username) && "123456".equals(password)) {
            //用户登录成功,将用户信息保存到session中
            session.setAttribute("loginUser", username);
            //登录成功,防止表单重复提交,重定向到主页面
            return "redirect:/main.html";
        } else {
            //登录失败
            map.put("msg", "用户名或密码错误!");
            return "login";

        }

    }
}
