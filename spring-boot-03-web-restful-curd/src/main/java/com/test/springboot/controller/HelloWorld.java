package com.test.springboot.controller;

import com.test.springboot.exception.UserNotExistException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.Map;

/**
 * @author: ybl
 * @create: 2019-05-11 16:46
 **/
//@RestController
@Controller
public class HelloWorld {

    @RequestMapping("hello")
    @ResponseBody //从请求体中获取参数,@PathVariable从请求路径上获取指定的参数,这个一般适用于result风格的请求
    public String hello(@RequestParam("user") String user) {
        if ("aaa".equals(user)) {
            throw new UserNotExistException();
        }
        return "hello world!";
    }

    @RequestMapping("success")
    public String success(Map<String, Object> map) {
        //thymeleaf模版使用,在类目录下的templates目录下创建html文件.模版就可以自动寻找到
        //注意,如果要跳转页面,前面的组件标签不可以使用restController了

        //非方法参数声明的数据,无法正常的传递到页面中
//        String iname = "叶良辰";

        map.put("hello", "<h1>你好</h1>");
        map.put("users", Arrays.asList("张三", "李四", "王五"));
        return "success";
    }

    //使用视图对象来传递参数时,注意在视图对象中指定视图(页面)的名称
    @RequestMapping("mv")
    public ModelAndView myMv(ModelAndView mv) {
        mv.addObject("hello", "<h1>你好</h1>");
        mv.addObject("users", Arrays.asList("张三", "李四", "王五"));
        mv.setViewName("modeAnView");

        return mv;
    }

    //手动指定主页
//    @RequestMapping({"/", "/login.html"})
//    public String index() {
//        return "index";
//    }
}
