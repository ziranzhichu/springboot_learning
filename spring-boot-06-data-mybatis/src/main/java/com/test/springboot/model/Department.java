package com.test.springboot.model;

/**
 * 
 * 
 * @author wcyong
 * 
 * @date 2019-06-25
 */
public class Department {
    private Integer id;

    private String departmentName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName == null ? null : departmentName.trim();
    }
}