package com.test.springboot.config;

import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;

/**
 * mybatis的配置
 *
 * @create: 2019-06-25 10:14
 **/
@org.springframework.context.annotation.Configuration
public class MybatisConfig {

    //开启驼峰命名，与数据中的字段名匹配
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return new ConfigurationCustomizer() {
            @Override
            public void customize(Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }
}
