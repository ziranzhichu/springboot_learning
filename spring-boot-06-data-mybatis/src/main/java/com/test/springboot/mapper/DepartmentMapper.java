package com.test.springboot.mapper;

import com.test.springboot.model.Department;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

//@Mapper
public interface DepartmentMapper {

    //该注解中的第一个参数为使用生成的值，第二个为字段名
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("INSERT INTO department(department_name) VALUES(#{departmentName})")
    int insert(Department record);


    @Select("SELECT * FROM department WHERE id = #{id}")
    Department selectByPrimaryKey(Integer id);

}