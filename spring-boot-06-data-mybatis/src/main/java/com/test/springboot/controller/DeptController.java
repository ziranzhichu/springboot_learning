package com.test.springboot.controller;

import com.test.springboot.mapper.DepartmentMapper;
import com.test.springboot.mapper.EmployeeMapper;
import com.test.springboot.model.Department;
import com.test.springboot.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @create: 2019-06-25 09:46
 **/
@RestController
public class DeptController {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @GetMapping("/dept/{id}")
    public Department getDepartment(@PathVariable("id") Integer id) {
        return departmentMapper.selectByPrimaryKey(id);
    }

    @GetMapping("/dept")
    public Department insert(Department department) {
        departmentMapper.insert(department);
        return department;
    }

    @GetMapping("/emp/{id}")
    public Employee getEmployee(@PathVariable("id") Integer id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    @GetMapping("/emp")
    public Employee insert(Employee employee) {
        employeeMapper.insert(employee);
        return employee;
    }
}
