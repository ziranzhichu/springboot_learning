package com.test.springboot.repository;

import com.test.springboot.bean.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @create: 2019-06-28 23:55
 **/
public interface BookRepository extends ElasticsearchRepository<Book,Integer> {

}
