package com.test.springboot.bean;

import org.springframework.data.elasticsearch.annotations.Document;

/**
 * @create: 2019-06-28 23:55
 **/
//使用elasticsearchRepository来操作索引时，实体类上要加上这个注解。表示使用哪个索引及类型
//indexName 索引库名
//type 表名
//shards 分片
//replicas 副本
@Document(indexName = "atguigu", type = "book", shards = 1, replicas = 0)
public class Book {

    private Integer id;
    private String bookName;
    private String author;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
