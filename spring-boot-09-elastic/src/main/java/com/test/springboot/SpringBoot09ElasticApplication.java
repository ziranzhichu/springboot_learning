package com.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot默认支持两种技术来和ES交互
 * 1、jest：默认不生效，需要导入jest的工具包(io.searchbox.client.JestClient)
 * 2、SpringBoot ElasticSearch：如果是springboot比较低的版本有可能会出现不适配的问题
 *      1)
 */
@SpringBootApplication
public class SpringBoot09ElasticApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot09ElasticApplication.class, args);
    }

}
