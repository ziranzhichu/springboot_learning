package com.test.springboot;

import com.test.springboot.bean.Article;
import com.test.springboot.bean.Book;
import com.test.springboot.repository.BookRepository;
import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot09ElasticApplicationTests {

    @Autowired
    JestClient jestClient;

    @Test
    public void contextLoads() {
        //1.给ES中(索引)保存一个文档
        Article article = new Article();
        article.setId(1);
        article.setAuthor("良辰");
        article.setTitle("良辰很厉害");
        article.setContent("千万不要跟良辰作对");

        //创建一个索引功能
        Index index = new Index.Builder(article).index("atguigu").type("news").build();

        //执行索引
        try {
            jestClient.execute(index);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试搜索
     */
    @Test
    public void search() {
        //使用表达式字符串
        String json = "{\n" +
                "    \"query\" : {\n" +
                "        \"match\" : {\n" +
                "            \"content\" : \"良辰\"\n" +
                "        }\n" +
                "    }\n" +
                "}";
        //创建搜索功能
        Search builder = new Search.Builder(json).addIndex("atguigu").addType("news").build();
        try {
            SearchResult result = jestClient.execute(builder);
            System.out.println(result.getJsonString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    BookRepository bookRepository;

    @Test
    public void test01() {
        Book book = new Book();
        bookRepository.index(book);
        //这个方法在执行时会报错。。。
    }
}
