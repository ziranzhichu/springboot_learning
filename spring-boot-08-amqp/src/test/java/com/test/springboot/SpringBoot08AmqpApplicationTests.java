package com.test.springboot;

import com.test.springboot.bean.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot08AmqpApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    AmqpAdmin amqpAdmin;

    /**
     * 创建exchange交换器
     * 创建queue队列
     */
    @Test
    public void createExchange() {
        //DirectExchange(); 点对点模式
        //FanoutExchange(); 广播模式
        //TopicExchange(); 主题模式
//        amqpAdmin.declareExchange(new DirectExchange("amqpadmin.exchange"));
//        System.out.println("创建完成！");

        //创建一个队列
        //amqpAdmin.declareQueue(new Queue("amqpadmin.queue", true));

        //创建一个绑定规则
        amqpAdmin.declareBinding(new Binding("amqpadmin.queue", Binding.DestinationType.QUEUE,
                "amqpadmin.exchange", "amqp.haha", null));
    }

    /**
     * 1、单播(点对点)
     */
    @Test
    public void contextLoads() {
        //方式1
        //message需要自己构造一个；定义消息体及消息头内容
        //rabbitTemplate.send(exchange, routingKey, message);

        //方式2
        //object默认当成消息体，只需要传入要发送的对象，自动序列化发送给rabbitMQ
        //rabbitTemplate.convertAndSend(exchange, routingKey, object);

        Map<String, Object> map = new HashMap<>();
        map.put("msg", "这是第一条消息");
        map.put("data", Arrays.asList("hello world", 123, true));
        //对象被默认序列化以后发送出去，序列化的规则是使用java默认的规则
//        rabbitTemplate.convertAndSend("exchange.direct", "atguigu.news", map);
        rabbitTemplate.convertAndSend("exchange.direct", "atguigu.news", new Book("java编程思想", "不知道"));

    }

    /**
     * 接收消息
     */
    @Test
    public void receive() {
        //通过队列的名称来接收消息
        Object o = rabbitTemplate.receiveAndConvert("atguigu.news");
        System.out.println(o.getClass());
        System.out.println(o);
    }

    /**
     * 广播
     */
    @Test
    public void sendMsg() {
        rabbitTemplate.convertAndSend("exchange.fanou", "", new Book("文体两开花", "六承铁章"));
    }

}
