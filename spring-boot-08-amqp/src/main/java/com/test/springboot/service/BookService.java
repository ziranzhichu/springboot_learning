package com.test.springboot.service;

import com.test.springboot.bean.Book;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @create: 2019-06-28 16:16
 **/
@Service
public class BookService {

    /**
     * 使用注解来监听消息队列，获取消息内容
     *
     * @param book
     */
    @RabbitListener(queues = "atguigu.news")
    public void receive(Book book) {
        System.out.println("接收到了消息！");
        System.out.println(book);
    }

    /**
     * 获取完整消息
     *
     * @param message
     */
    @RabbitListener(queues = "atguigu")
    public void receive02(Message message) {
        System.out.println(message.getBody());
        System.out.println(message.getMessageProperties());
    }
}
