package com.test.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: ybl
 * @create: 2019-05-09 17:04
 **/
@RestController
public class HelloWorld {

    //日志,使用springboot自带的slf4j
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);

    @RequestMapping("hello")
    public String hello() {
        logger.info("有请求请求了hello接口");        return "hello world boot02! ";
    }
}
