package com.test.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot03LoggingApplicationTests {

    @Test
    public void contextLoads() {
        //打印日志
        Logger logger = LoggerFactory.getLogger(getClass());

        //打印日志的级别由低到高
        logger.trace("这是trace日志.."); //跟踪日志
        logger.debug("这是debug日志"); //调试日志
        logger.info("这是info日志"); //自定义日志,开发中常用的日志级别
        logger.warn("这是warn日志"); //警告日志
        logger.error("这是error日志"); //异常日志
    }

}
