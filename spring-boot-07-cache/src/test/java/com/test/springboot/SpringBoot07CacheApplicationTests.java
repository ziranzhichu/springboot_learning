package com.test.springboot;

import com.test.springboot.bean.Employee;
import com.test.springboot.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot07CacheApplicationTests {

    @Autowired
    EmployeeMapper employeeMapper;

    //该redis模版用来操作k-v都是字符串的
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    //这个操作k-v都是对象的
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedisTemplate<Object, Employee> empRedisTemplate;

    @Test
    public void test01() {
        //stringRedisTemplate.opsForValue(); //操作字符串类型
        //stringRedisTemplate.opsForHash(); //操作hash类型
        //stringRedisTemplate.opsForList(); //操作list集合类型
        //stringRedisTemplate.opsForSet(); //操作set类型
        //stringRedisTemplate.opsForZSet(); //操作有序集合

        //给redis中保存一个数据
        //stringRedisTemplate.opsForValue().append("msg", "我怎能不变态");
        //获取数据
        //String msg = stringRedisTemplate.opsForValue().get("msg");
        //System.out.println(msg);

//        stringRedisTemplate.opsForList().leftPush("myList","1");
//        stringRedisTemplate.opsForList().leftPush("myList","2");
    }

    /**
     * 测试保存对象
     */
    @Test
    public void test02() {
        Employee emp = employeeMapper.getEmpById(1);
        //默认如果保存对象，使用jdk序列化机制，序列化后的数据保存到redis
//        redisTemplate.opsForValue().set("emp-01", emp);

        //将数据以json的方式保存
        //1.自己将对象转为json
        //2.自定义redisTemplate默认的序列化机制
        empRedisTemplate.opsForValue().set("emp-02", emp);
    }

    @Test
    public void contextLoads() {
        Employee emp = employeeMapper.getEmpById(1);
        System.out.println(emp);

    }

}
