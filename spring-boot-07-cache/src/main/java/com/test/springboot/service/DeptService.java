package com.test.springboot.service;

import com.test.springboot.bean.Department;
import com.test.springboot.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.stereotype.Service;

/**
 * @create: 2019-06-27 17:23
 **/
@Service
@CacheConfig(cacheNames = "dept")
public class DeptService {

    @Autowired
    private DepartmentMapper departmentMapper;

    //这里因为CacheManager自己声名过一个，所以这里要指定名称注入
    @Qualifier("cacheManager")
    @Autowired
    CacheManager redisCacheManager;

    @Cacheable(key = "#id")
    public Department getDeptById(Integer id) {
        System.out.println("查询了" + id + "号部门");
        Department dept = departmentMapper.getDeptById(id);

        //使用编码的方式使用缓存
//        Cache cache = redisCacheManager.getCache("dept");
//        cache.put("dept:" + id, dept);

        return dept;
    }
}
