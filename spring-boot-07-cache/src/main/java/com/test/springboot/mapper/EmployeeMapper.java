package com.test.springboot.mapper;

import com.test.springboot.bean.Employee;
import org.apache.ibatis.annotations.*;

/**
 * @create: 2019-06-25 19:09
 **/
@Mapper
public interface EmployeeMapper {

    @Select("SELECT * FROM employee WHERE id = #{id}")
    Employee getEmpById(Integer id);

    @Update("UPDATE employee SET lastName = #{lastName}, email = #{email}, gender = #{gender}, d_id = #{dId} where id = #{id}")
    void updateEmp(Employee employee);

    @Delete("delete from employee where id = #{id}")
    void deleteEmp(Integer id);

    @Insert("insert into employee(lastName, email, gender, dId) values(#{lastName}, #{email}, #{gender}, #{dId})")
    void saveEmp(Employee employee);

    @Select("SELECT * FROM employee WHERE lastName = #{lastName}")
    Employee getEmpByLastName(String lastName);
}
