package com.test.springboot.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * 缓存的自定义配置类
 *
 * @create: 2019-06-26 22:09
 **/
@Configuration
public class MyCacheConfig {

    /**
     * 自定义keyGenerator；key生成器
     *
     * @return
     */
    @Bean("myKeyGenerator") //指定名称
    public KeyGenerator keyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object target, Method method, Object... params) {
                return method.getName() + "[" + Arrays.asList(params).toString() + "]";
            }
        };
    }
}
