package com.test.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 一、搭建基本环境
 * 1.导入数据库文件，创建department和employee表
 * 2.创建javaBean，
 * 3.整合mybatis，操作数据库
 *         1）配置数据源
 *         2）使用注解版的mybatis
 *              (1)、@mapperScan指定需要扫描的mapper接口的位置
 * 二、快速体验缓存
 *  步骤
 *  1.开启基于注解的缓存；@EnableCaching
 *  2.标注缓存注解
 *  @Cacheable
 *  @CacheEvict
 *  @CachePut
 *
 *  默认使用的是ConcurrentMapCacheManager == ConcurrentMapCache来作为缓存保存在ConcurrentMap中
 *
 *  三、整合redis作为缓存
 *  1.安装redis，使用docker;
 *  2.引入redis的starter;
 *  3.配置redis，如果项目中配置了redis来作为缓存，那么原来基于concurrentMap作为缓存的数据现在会保存到redis中，而原来基于map的配置现在也会生效；
 *  4.测试缓存
 *      原理：CacheManager === Cache 缓存组件来实际给缓存中存取数据
 *      1)引入redis的starter之后，容器中保存的就是RedisCacheManager;
 *      2)RedisCacheManager帮我们创建RedisCache来作为缓存组件，RedisCache通过操作redis来缓存数据；
 *      3)默认保存数据 k-v 都是object时，利用序列化(jdk默认)来保存
 *          如何保存为json
 *          1> 引入了redis的starter,CacheManager变为RedisCacheManager;
 *          2> 默认创建的RedisCacheManager操作redis时使用RedisTemplast<object, object>;
 *          3> RedisTemplast<object, object>是默认使用jdk的序列化机制;
 *      4)自定义CacheManager;
 */
@MapperScan("com.test.springboot.mapper") //扫描mapper接口类
@SpringBootApplication
@EnableCaching
public class SpringBoot07CacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot07CacheApplication.class, args);
    }

}
