package com.test.elasticsearch;

import com.test.elasticsearch.pojo.Item;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @create: 2019-07-15 18:42
 **/
public interface ItemRepository extends ElasticsearchRepository<Item, Long> {

    /**
     * 根据标题来进行模糊查询
     *
     * @param title
     * @return
     */
    List<Item> findByTitle(String title);

    /**
     * 一个参数在两个值之间进行查询
     *
     * @param d1
     * @param d2
     * @return
     */
    List<Item> findByPriceBetween(Double d1, Double d2);
}
