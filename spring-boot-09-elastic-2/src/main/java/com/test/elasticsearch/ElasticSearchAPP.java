package com.test.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @create: 2019-07-15 17:09
 **/
@SpringBootApplication
public class ElasticSearchAPP {
    public static void main(String[] args) {
        SpringApplication.run(ElasticSearchAPP.class);
    }
}
