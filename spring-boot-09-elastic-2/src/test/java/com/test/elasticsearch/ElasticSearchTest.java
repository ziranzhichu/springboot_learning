package com.test.elasticsearch;

import com.test.elasticsearch.pojo.Item;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.metrics.avg.InternalAvg;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @create: 2019-07-15 16:47
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class ElasticSearchTest {

    /**
     * spring data提供给我们使用的elastic模版
     */
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void indexTest() {
        //创建索引
        this.elasticsearchTemplate.createIndex(Item.class);
        //创建映射
        this.elasticsearchTemplate.putMapping(Item.class);
    }

    @Test
    public void testCreate() {
        //保存一条数据,如果两条数据id相同。那么则视为更新一条数据
        this.itemRepository.save(new Item(1L, "小米手机8", " 手机", "小米", 3599.00, "http://image.leyou.com/13123.jpg"));

//        List<Item> list = new ArrayList<>();
//        list.add(new Item(2L, "坚果手机R1", " 手机", "锤子", 3699.00, "http://image.leyou.com/123.jpg"));
//        list.add(new Item(3L, "华为META10", " 手机", "华为", 4499.00, "http://image.leyou.com/3.jpg"));
//        // 接收对象集合，实现批量新增
//        this.itemRepository.saveAll(list);
    }

    /**
     * 查询
     */
    @Test
    public void testFind() {
        //根据id查询
//        Optional<Item> item = this.itemRepository.findById(1l);
//        System.out.println(item.get());

        //查询所有，并按指定字段作排序
        Iterable<Item> all = this.itemRepository.findAll(Sort.by("price").descending());
        //使用jdk1.8的新特性来打印
        all.forEach(System.out::println);

    }

    @Test
    public void testFindByTitle() {
        List<Item> title = this.itemRepository.findByTitle("小米");
        title.forEach(System.out::println);
    }

    @Test
    public void findByPriceBetween() {
        List<Item> items = this.itemRepository.findByPriceBetween(3600.00, 4500.00);
        items.forEach(System.out::println);
    }

    @Test
    public void indexList() {
        List<Item> list = new ArrayList<>();
        list.add(new Item(1L, "小米手机7", "手机", "小米", 3299.00, "http://image.leyou.com/13123.jpg"));
        list.add(new Item(2L, "坚果手机R1", "手机", "锤子", 3699.00, "http://image.leyou.com/13123.jpg"));
        list.add(new Item(3L, "华为META10", "手机", "华为", 4499.00, "http://image.leyou.com/13123.jpg"));
        list.add(new Item(4L, "小米Mix2S", "手机", "小米", 4299.00, "http://image.leyou.com/13123.jpg"));
        list.add(new Item(5L, "荣耀V10", "手机", "华为", 2799.00, "http://image.leyou.com/13123.jpg"));
        // 接收对象集合，实现批量新增
        itemRepository.saveAll(list);
    }

    /**
     * 高级查询
     */
    @Test
    public void testSearch() {
        //通过查询构建器工具构建查询条件
        MatchQueryBuilder query = QueryBuilders.matchQuery("title", "手机");
        //执行查询，获取结果
        Iterable<Item> items = this.itemRepository.search(query);
        items.forEach(System.out::println);
    }

    /**
     * 自定义查询
     */
    @Test
    public void testNative() {
        //构建自定义查询构建器
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //添加基本查询条件
        queryBuilder.withQuery(QueryBuilders.matchQuery("title", "手机"));
        //执行查询，获取分页结果集
        Page<Item> itemPage = this.itemRepository.search(queryBuilder.build());
        System.out.println(itemPage.getTotalPages());
        System.out.println(itemPage.getTotalElements());
        itemPage.forEach(System.out::println);
    }

    /**
     * 分页查询
     */
    @Test
    public void testPage() {
        //构建自定义查询构建器
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //添加基本查询条件
        queryBuilder.withQuery(QueryBuilders.matchQuery("category", "手机"));
        //设置分页条件，页码从0开始
        queryBuilder.withPageable(PageRequest.of(1, 2));
        //执行查询，获取分页结果集
        Page<Item> itemPage = this.itemRepository.search(queryBuilder.build());
        System.out.println(itemPage.getTotalPages());
        System.out.println(itemPage.getTotalElements());
        itemPage.forEach(System.out::println);
    }

    /**
     * 排序查询
     */
    @Test
    public void testSort() {
        //构建自定义查询构建器
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //添加基本查询条件
        queryBuilder.withQuery(QueryBuilders.matchQuery("category", "手机"));
        //设置排序字段及规则
        queryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.DESC));
        //执行查询，获取分页结果集
        Page<Item> itemPage = this.itemRepository.search(queryBuilder.build());
        System.out.println(itemPage.getTotalPages());
        System.out.println(itemPage.getTotalElements());
        itemPage.forEach(System.out::println);
    }

    /**
     * 聚合：
     * 相当于sql中的根据某个字段来分组一样，在返回的结果中指定你需要的字段及不需要的字段。
     * 类似于分组后的结果
     */
    @Test
    public void testAggs() {
        //初始化自定义查询构建器
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //添加聚合
        queryBuilder.addAggregation(AggregationBuilders.terms("brandAgg").field("brand"));
        //添加结果过滤集，不包括任何字段
        queryBuilder.withSourceFilter(new FetchSourceFilter(new String[]{}, null));
        //执行聚合查询
        AggregatedPage<Item> items = (AggregatedPage<Item>) this.itemRepository.search(queryBuilder.build());
        //解析聚合结果集，根据聚合的类型以及字段的类型要进行强转。brand字段是字符串类型
        //聚合类型-词条聚合，brandAgg通过聚合名称获取聚合对象
        StringTerms brandAgg = (StringTerms) items.getAggregation("brandAgg");
        //获取桶的集合
        List<StringTerms.Bucket> buckets = brandAgg.getBuckets();
        buckets.forEach(bucket -> {
            System.out.println(bucket.getKeyAsString());
            System.out.println(bucket.getDocCount());
        });
    }

    /**
     *
     */
    @Test
    public void testSubAggs() {
        //初始化自定义查询构建器
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //添加聚合
        queryBuilder.addAggregation(AggregationBuilders.terms("brandAgg").field("brand")
                .subAggregation(AggregationBuilders.avg("price_avg").field("price")));
        //添加结果过滤集，不包括任何字段
        queryBuilder.withSourceFilter(new FetchSourceFilter(new String[]{}, null));
        //执行聚合查询
        AggregatedPage<Item> items = (AggregatedPage<Item>) this.itemRepository.search(queryBuilder.build());
        //解析聚合结果集，根据聚合的类型以及字段的类型要进行强转。brand字段是字符串类型
        //聚合类型-词条聚合，brandAgg通过聚合名称获取聚合对象
        StringTerms brandAgg = (StringTerms) items.getAggregation("brandAgg");
        //获取桶的集合
        List<StringTerms.Bucket> buckets = brandAgg.getBuckets();
        buckets.forEach(bucket -> {
            System.out.println(bucket.getKeyAsString());
            System.out.println(bucket.getDocCount());
            Aggregations aggregations = bucket.getAggregations();
            Map<String, Aggregation> asMap = aggregations.asMap();
            InternalAvg price_avg = (InternalAvg) asMap.get("price_avg");
            System.out.println(price_avg.getValue());
        });
    }
}
