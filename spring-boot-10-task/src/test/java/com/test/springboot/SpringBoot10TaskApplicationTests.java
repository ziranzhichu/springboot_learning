package com.test.springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot10TaskApplicationTests {

    /**
     * java发送邮件模版
     */
    @Autowired
    JavaMailSenderImpl mailSender;

    /**
     * 发送简单邮件
     */
    @Test
    public void contextLoads() {
        SimpleMailMessage message = new SimpleMailMessage();
        //设置邮件信息
        message.setSubject("通知：我怎能不变态"); //标题
        message.setText("非处娶不得"); // 邮件内容
        message.setTo("y453042826@163.com"); //收件人邮箱
        message.setFrom("453042826@qq.com"); //发件人

        mailSender.send(message);
    }

    /**
     * 发送复杂邮件，带附件的邮件
     */
    @Test
    public void test01() throws MessagingException {
        //1.创建一个复杂的消息邮件
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        //设置邮件信息
        helper.setSubject("通知：我怎能不变态"); //标题
        helper.setText("<b style = 'color:red'>非处娶不得</b>", true); // 邮件内容,html代码
        helper.setTo("y453042826@163.com"); //收件人邮箱
        helper.setFrom("453042826@qq.com"); //发件人

        //上传附件
        helper.addAttachment("1.jpg", new File("F:\\7Q5-k7bmXxZ9aT3cS1z4-140.jpg")); //图片
//        helper.addAttachment("2.mp4", new File("F:\\4.mp4")); //mp4文件,可能因为过大而发送失败

        mailSender.send(mimeMessage);
    }
}
