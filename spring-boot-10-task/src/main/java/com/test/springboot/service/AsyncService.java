package com.test.springboot.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 使用异常任务
 *
 * @create: 2019-06-29 14:21
 **/
@Service
public class AsyncService {

    /**
     * 告诉spring这个方法是一个异步方法，使用另一个线程来开启
     */
    @Async
    public void hello() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据处理中...");
    }
}
