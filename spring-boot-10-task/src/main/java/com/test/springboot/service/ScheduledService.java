package com.test.springboot.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 使用定时任务
 *
 * @create: 2019-06-29 14:31
 **/
@Service
public class ScheduledService {

    /**
     * second(秒), minute(分), hour(时), day of month(日), month(月), day of week(周几)
     * 0 * * * * MON-FRI
     */
    @Scheduled(cron = "0 * * * * *")
    public void task() {
        System.out.println("执行了定时任务...");
    }
}
