package com.test.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 使用jdbcTemplate模版来处理一些简单的查询
 *
 * @create: 2019-06-23 23:16
 **/
@Controller
public class QueryTable {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping("query")
    @ResponseBody
    public Map<String, Object> querry() {
        //使用sql语句
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from department");
        return list.get(0);
    }
}
