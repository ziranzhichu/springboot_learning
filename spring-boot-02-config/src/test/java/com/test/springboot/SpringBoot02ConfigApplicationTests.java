package com.test.springboot;

import com.test.springboot.bean.Person;
import com.test.springboot.bean.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot02ConfigApplicationTests {

    @Autowired
    private Person person;

    @Autowired
    private Student student;

    @Autowired
    ApplicationContext aic;

    @Test
    public void contextLoads() {
        System.out.println(person);

    }

    //PropertySource
    //propertySource注解,加载指定配置文件
    @Test
    public void test02() {
        System.out.println(student);
    }

    //使用importResource注解的测试
    @Test
    public void test03() {
        System.out.println(aic.containsBean("helloService"));
    }

}
