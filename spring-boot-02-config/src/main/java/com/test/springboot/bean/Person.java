package com.test.springboot.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 将配置文件中配置的每个属性的值,映射到对象组件中
 * <p>
 * ConfigurationProperties:告诉springboot将本类中的所有属性和配置文件中相关的配置进行绑定;
 * 默认从全局配置文件中获取值
 * prefix = "person" 与配置文件中的哪个配置前缀进行一一映射;哪个配置的名称
 * 如果需要开启configurationProperties注解,需要导入对应的依赖
 * 只有这个组件是容器中的组件才可以使用容器提供的功能,ConfigurationProperties注解才可以生效,前面与spring的组件注解一起使用
 * 比如Component,
 * <p>
 * Validated:jsr303数据校验,可以进行一些简单的数据校验,看是否符合要求.只能搭配ConfigurationProperties注解使用才能够生效
 *
 * @author: ybl
 * @create: 2019-05-09 21:55
 **/
@Component
@ConfigurationProperties(prefix = "person")
@Validated
public class Person {

    /*
    使用@value注解来获取配置文件中的值,这里中间注意要使用.来连接,类似于properties文件中的写法.而不能使用:号
    格式${}
     */
//    @Value("${person.lastName}")
//    @Email Validated数据校验时该注解表示这个值只能是一个邮箱地址
    private String lastName;

    //使用表达式来计算值
    //格式是#{}
//    @Value("#{11*2}")
    private Integer age;

    //自定义值
//    @Value("true")
    private Boolean boss;
    private Date birth;

    //    @Value("${person.maps}") @Value不支持复杂类型数据,这里不能获取map集合的值
    private Map<String, Object> maps;
    private List<Object> lists;

    private Dog dog;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getBoss() {
        return boss;
    }

    public void setBoss(Boolean boss) {
        this.boss = boss;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Map<String, Object> getMaps() {
        return maps;
    }

    public void setMaps(Map<String, Object> maps) {
        this.maps = maps;
    }

    public List<Object> getLists() {
        return lists;
    }

    public void setLists(List<Object> lists) {
        this.lists = lists;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    @Override
    public String toString() {
        return "Person{" +
                "lastName='" + lastName + '\'' +
                ", age=" + age +
                ", boss=" + boss +
                ", birth=" + birth +
                ", maps=" + maps +
                ", lists=" + lists +
                ", dog=" + dog +
                '}';
    }
}
