package com.test.springboot.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: ybl
 * @create: 2019-05-09 17:04
 **/
@RestController
public class HelloWorld {

    @Value("${person.lastName}")
    private String name;

    @RequestMapping("hello")
    public String hello() {
        return "hello world quick! " + name;
    }
}
