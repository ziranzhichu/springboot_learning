package com.test.springboot.config;

import com.test.springboot.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration:表示该类一个配置类,就是来代替之前的spring配置文件
 *
 * @author: ybl
 * @create: 2019-05-09 23:55
 **/
@Configuration
public class MyAPPConfig {

    /*
    bean:将方法的返回值添加到容器中,容器中这个组件默认的id就是方法的名称
     */
    @Bean
    public HelloService helloService() {
        System.out.println("配置类@Bean给容器中添加组件了");
        return new HelloService();
    }
}
