package com.test.springboot.controller;

import com.test.springboot.entity.User;
import com.test.springboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @create: 2019-06-25 16:12
 **/
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Integer id) {
        return userRepository.getOne(id);
    }

    @GetMapping("/user")
    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
