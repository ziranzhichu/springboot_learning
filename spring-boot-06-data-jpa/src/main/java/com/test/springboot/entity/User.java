package com.test.springboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * @create: 2019-06-25 15:45
 **/
//使用jpa注解配置映射关系
@Entity //告诉jpa这是一个实体类，与数据库中表映射
@Table(name = "tb_user") //指定与哪个表映射，如果省略默认表名小写：user
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})  //在将实体类转化成json时忽略类中值为Null的属性
public class User {

    @Id //主键
    @GeneratedValue(strategy = GenerationType.IDENTITY) //自增策略
    private Integer id;

    //其它字段，如果省略注解则会默认名称为小写，如果想这个字段不是表中的则需要添加其它注解
    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "email", length = 30)
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
