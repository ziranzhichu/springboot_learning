package com.test.springboot.repository;

import com.test.springboot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * user实体类的DAO操作类；
 * 继承japRepository来完成
 *
 * @create: 2019-06-25 15:53
 **/
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


}
