package com.example.springboot.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * security的配置类
 *
 * @create: 2019-06-29 16:10
 **/
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 定义授权规则
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);

        //定制请求的授权规则
        http.authorizeRequests().antMatchers("/").permitAll()
                //给某个请求添加需要的角色
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");

        //开启自动配置的登录功能；效果，如果没有登录，没有权限就会来到登录页面
        http.formLogin().usernameParameter("user").passwordParameter("pwd")
                .loginPage("/userlogin"); //loginPage指定自己的登录页面
        //1. /login请求来到登录页
        //2. 登录失败重定向到/login?error页面
        //3. 更新详细规定
        //4. 默认post形式的/login代表处理登录
        //5. 一但定制loginPage,那么loginPage的post请求就是登录;在之后所有的登录请求名都应该是你自己定制的请求名称/userLogin

        //开启自动配置注销功能
        http.logout().logoutSuccessUrl("/"); //logoutSuccessUrl 注销成功后跳转的请求
        //1.访问/logout 表示用户退出
        //2.注销成功会返回/login?logout 页面

        //开启记住我功能
        http.rememberMe().rememberMeParameter("remember"); //rememberMeParameter定制一个自己的记住我请求，名称为remember
        //1.登录成功以后，将cookie发给浏览器保存，以后访问页面带上这个cookie，只要通过验证就可以免登录
        //2.点击注销，删除cookie
    }

    /**
     * 定义认证规则
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        super.configure(auth);

        auth.inMemoryAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("zhangsan").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1", "VIP2")
                .and()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("lisi").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1", "VIP3")
                .and()
                .passwordEncoder(new BCryptPasswordEncoder())
                .withUser("wangwu").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP2", "VIP3");

    }
}
