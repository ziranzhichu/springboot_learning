package com.example.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1.引入spring security
 * 2.编写spring security的配置类
 *      @EnableWebSecurity
 *      public class MySecurityConfig extends WebSecurityConfiguration
 * 3.控制请求的访问权限
 */
@SpringBootApplication
public class SpringBoot11SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot11SecurityApplication.class, args);
    }

}
